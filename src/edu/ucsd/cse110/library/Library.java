package edu.ucsd.cse110.library;

import java.util.Hashtable;

public class Library {
	CheckFeeEnterprise checkAndFeePackage;
	Hashtable<String,Member> members = new Hashtable<String,Member>();
	Hashtable<String,Publication> publications = new Hashtable<String,Publication>();
	
	public Library()
	{
			checkAndFeePackage = new CheckFeeEnterprise();
	}
	
	public Member getMember(String name)
	{
		return members.get(name);
	}
	
	public void addMember(Member m)
	{
		members.put(m.getName(),m);
	}
	
	public void removeMember(String name)
	{
		members.remove(name);
	}
	
	public Publication getPublication(String name)
	{
		return publications.get(name);
	}
	
	public void removePublication(String name)
	{
		publications.remove(name);
	}
	
	public void addPublication(Publication p)
	{
		publications.put(p.toString(),p);
	}
	
	public void checkoutPublication(Member m, Publication p) {
		checkAndFeePackage.checkoutPublication(m, p);
	}

	public void returnPublication(Publication p) {
		checkAndFeePackage.returnPublication(p);		
	}

	public double getFee(Member m) {
		return checkAndFeePackage.getFee(m);	
	}

	public boolean hasFee(Member m) {
		return checkAndFeePackage.hasFee(m);
	}

}
