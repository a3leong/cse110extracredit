package edu.ucsd.cse110.library;

import java.time.LocalDate;

public class CheckFeeEnterprise implements Facade {

	public CheckFeeEnterprise() // Empty constructor for instance
	{
		
	}
	public void checkoutPublication(Member m, Publication p) {
		p.checkout(m,LocalDate.now());
	}

	public void returnPublication(Publication p) {
		p.pubReturn(LocalDate.now());		
	}

	public double getFee(Member m) {
		return m.getDueFees();
	}

	public boolean hasFee(Member m) {
		return (m.getDueFees()>0);
	}

}
